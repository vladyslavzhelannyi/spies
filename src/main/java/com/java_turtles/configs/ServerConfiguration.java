package com.java_turtles.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.controllers.AuthorizationController;
import com.java_turtles.controllers.RecoverController;
import com.java_turtles.controllers.RegController;
import com.java_turtles.utils.validation.RegistrationValidation;
import com.java_turtles.utils.db_utils.IUsersDbUtility;
import com.java_turtles.utils.db_utils.UsersDbUtility;
import com.java_turtles.utils.validation.AuthorizationValidation;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServerConfiguration {

    public Server buildServer(){
        Server server = new Server();
        ObjectMapper objectMapper = new ObjectMapper();
        IUsersDbUtility usersDbUtility = new UsersDbUtility();
        RegistrationValidation registrationValidation = new RegistrationValidation(usersDbUtility);
        AuthorizationValidation authorizationValidation = new AuthorizationValidation(usersDbUtility);
        ServerConnector serverConnector = new ServerConnector(server);
        serverConnector.setPort(8090);
        server.setConnectors(new ServerConnector[]{serverConnector});
        ServletContextHandler context = new ServletContextHandler();
        ServletHolder regServletHolder = new ServletHolder("regHolder", new RegController(objectMapper, registrationValidation, usersDbUtility));
        ServletHolder recoverServletHolder = new ServletHolder("recoverHolder", new RecoverController(objectMapper));
        ServletHolder authServletHolder = new ServletHolder("authHolder",
                new AuthorizationController(objectMapper, authorizationValidation));
        context.addServlet(authServletHolder, "/auth");
        context.addServlet(regServletHolder, "/reg");
        context.addServlet(recoverServletHolder, "/recover");
        HandlerCollection hc = new HandlerCollection();
        hc.setHandlers(new Handler[]{context});
        server.setHandler(hc);
        return server;
    }
}

