package com.java_turtles.configs;

import com.java_turtles.exceptions.FailedConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnectionConfig {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/crud";
    private static final String USER = "postgres";
    private static final String PASSWORD = "mva2022";

    public static Connection getConnectionToPostgres() throws FailedConnectionException {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }
}