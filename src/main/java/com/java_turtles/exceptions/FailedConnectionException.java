package com.java_turtles.exceptions;

public class FailedConnectionException extends Exception {

    public FailedConnectionException(String message) {
        super(message);
    }
}
