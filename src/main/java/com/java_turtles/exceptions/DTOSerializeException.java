package com.java_turtles.exceptions;

public class DTOSerializeException extends Exception{

    public DTOSerializeException(String message) {
        super(message);
    }

}
