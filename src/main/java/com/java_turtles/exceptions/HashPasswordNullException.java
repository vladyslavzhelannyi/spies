package com.java_turtles.exceptions;

public class HashPasswordNullException extends Exception{

    public static final String message = "HashPasswordNullException";
    public HashPasswordNullException() {
        super(message);
    }
}
