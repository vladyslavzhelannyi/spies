package com.java_turtles;

import com.java_turtles.configs.ServerConfiguration;
import org.eclipse.jetty.server.Server;


public class Run {
    public static void main(String[] args) {
        ServerConfiguration serverConfiguration = new ServerConfiguration();
        Server server = serverConfiguration.buildServer();
        try{
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
