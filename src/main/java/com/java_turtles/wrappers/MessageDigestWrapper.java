package com.java_turtles.wrappers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MessageDigestWrapper {

    public MessageDigest getWithInstance() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("SHA-512");
    }
}
