package com.java_turtles.wrappers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class InputStreamWrapper {
    public FileInputStream getFileInputStream(String path) throws FileNotFoundException {
        return new FileInputStream(path);
    }
}
