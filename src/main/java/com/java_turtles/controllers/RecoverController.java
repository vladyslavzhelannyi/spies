package com.java_turtles.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.models.entities.RecoverDTO;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class RecoverController extends HttpServlet {

    private ObjectMapper objectMapper ;

    public RecoverController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("init recover");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("postRecover");

        String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator())); // не для секретов, данные в виде объектов
        System.out.println(body);
        RecoverDTO recoverDto = objectMapper.readValue(body, RecoverDTO.class);
        System.out.println(recoverDto);


    }
}
