package com.java_turtles.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.models.entities.AuthDTO;
import com.java_turtles.utils.JWTUtil;
import com.java_turtles.utils.validation.AuthorizationValidation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

import static com.java_turtles.constants.ValidationConstants.*;

public class AuthorizationController extends HttpServlet{
    private ObjectMapper objectMapper;
    private AuthorizationValidation authorizationValidation;
//    private JWTUtil jwtUtil = new JWTUtil("no matter");

    public AuthorizationController(ObjectMapper objectMapper, AuthorizationValidation authorizationValidation) {
        this.objectMapper = objectMapper;
        this.authorizationValidation = authorizationValidation;
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        // Ошибка: не смогли сериализовать, неправильный формат
        AuthDTO authDTO;
        try {
            authDTO = objectMapper.readValue(body, AuthDTO.class);
            System.out.println(authDTO);
        } catch (Exception e) {
            System.out.println(MSG_IMPOSSIBLE_SERIALIZATION);
            resp.getWriter().write(MSG_INCORRECT_DATA);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        String validateResponse = authorizationValidation.getValidateResponse(authDTO);

        if (!validateResponse.equals(MSG_SUCCESS)){
            resp.getWriter().write(validateResponse);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        } else {
//            resp.setHeader("access_token", jwtUtil.createJWT(authDTO.getLogin(), "", "", 4));
            resp.setStatus(HttpServletResponse.SC_OK);
        }
    }
}
