package com.java_turtles.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.models.User;
import com.java_turtles.models.entities.RegistrationDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;
import com.java_turtles.utils.validation.RegistrationValidation;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RegController extends HttpServlet {

    private ObjectMapper objectMapper;
    private RegistrationValidation registrationValidation;
    private IUsersDbUtility usersDbUtility;

    private static final String RESPONSE_SUCCESS = "OK";
    private static final String RESPONSE_INCORRECT_DATA = "Incorrect data";

    public RegController(ObjectMapper objectMapper, RegistrationValidation registrationValidation, IUsersDbUtility usersDbUtility) {
        this.objectMapper = objectMapper;
        this.registrationValidation = registrationValidation;
        this.usersDbUtility = usersDbUtility;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("init reg");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("post"); //sout!

        BufferedReader bufferedReader = request.getReader();
        Stream<String> stream = bufferedReader.lines();


        String body = stream.collect(Collectors.joining(System.lineSeparator())); // не для секретов, данные в виде объектов
        System.out.println(body);

        RegistrationDTO regDTO;

        try {
            regDTO = objectMapper.readValue(body, RegistrationDTO.class);
            System.out.println(regDTO);
        }
        catch (JsonProcessingException e){
            System.out.println("cast not possible");
            response.getWriter().write(RESPONSE_INCORRECT_DATA);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        String validateResponse = registrationValidation.getValidateResponse(regDTO);

        if (!validateResponse.equals(RESPONSE_SUCCESS)){
            response.getWriter().write(validateResponse);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        User user = new User(regDTO.getLogin(), regDTO.getPassword(), regDTO.getEmail(), regDTO.getPhoneNumber(), regDTO.getCompany());

        System.out.println(user);

        usersDbUtility.addUser(user);

        response.getWriter().write(RESPONSE_SUCCESS);
        response.setStatus(HttpServletResponse.SC_CREATED);

    }
}


//лучше отправлять строку или создать объект чтоб отправлять строку и ошибку?
// рег выр на каждое поле,
// логин от 8-16 только буквы и цифры
// пароль 8-16
// емейл
// телефон междунар. форм.
// компания

//        System.out.println(req.getHeader("headernew")); //отправка идет только с
//        // боди, это для токенов. Для секретных данных. то от кого и т.д., ключ
//        System.out.println(req.getParameter("key1")); //для урла(?), о пользователе, откуда пришел.
//
//        if (authDto != null){
//            resp.getWriter().write("success");
//            resp.setStatus(HttpServletResponse.SC_CREATED);
//        }
//        else {
//            resp.getWriter().write("fail");
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//        }

