package com.java_turtles.constants;

public class ValidationConstants {

    public static final String MSG_SUCCESS = "OK";

    public static final String MSG_INCORRECT_DATA = "Incorrect data";

    public static final String MSG_IMPOSSIBLE_SERIALIZATION = "Serialization is impossible";

    public static final String MSG_INVALID_EMPTY_FIELD = "Fields cannot be empty";

    public static final String MSG_INVALID_NULL = "Fields cannot be null";

    public static final String MSG_INVALID_CREDENTIALS = "Invalid credentials";

    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";

    public static final String MSG_INVALID_INCORRECT_PASSWORD = "Incorrect password";

    public static final String MSG_INVALID_CONFIRM_PASSWORD_MISMATCH = "Password mismatch";

    public static final String MSG_INVALID_INCORRECT_EMAIL = "Incorrect e-mail";

    public static final String MSG_INVALID_INCORRECT_NUMBER = "Incorrect number";

    public static final String MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS = "Exceeded number of characters";

    public static final String REGEX_CORRECT_PASSWORD = "[A-Za-z0-9]{8,16}";

    public static final String REGEX_CORRECT_PASSWORD_2 = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})";

    public static final String REGEX_CORRECT_LOGIN = "[A-Za-z0-9]{8,16}";

    public static final String REGEX_CORRECT_EMAIL = "^[a-zA-Z0-9+_\\.-]{1,32}+@[a-zA-Z0-9-]{1,32}\\.[a-zA-Z0-9-]{2,6}+$";

    public static final String REGEX_CORRECT_PHONE_NUMBER = "\\+[1-9]\\d{7,14}"; //min 8, max 15

    public static final int COMPANY_NAME_MAX_SYMBOLS = 80;

}
