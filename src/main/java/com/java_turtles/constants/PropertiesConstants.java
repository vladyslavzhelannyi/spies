package com.java_turtles.constants;

public class PropertiesConstants {
    public static final String JWT_PROPERTY = "jwt_secret_key";

    public static final String PROPERTIES_PATH = "src/main/resources/config.properties";

    public static final String SALT_PROPERTY = "salt";

    public static final String SALT_PATH = "src/main/resources/salt.properties";

}
