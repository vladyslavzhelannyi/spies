package com.java_turtles.utils;

import com.java_turtles.wrappers.InputStreamWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.java_turtles.constants.PropertiesConstants.*;

public class PropertiesUtil {
    private InputStreamWrapper inputStreamWrapper;
    private Properties properties;

    public PropertiesUtil(InputStreamWrapper inputStreamWrapper, Properties properties) {
        this.inputStreamWrapper = inputStreamWrapper;
        this.properties = properties;
    }

    public String getJwtSecretKey() {
        return getValue(JWT_PROPERTY, PROPERTIES_PATH);
    }

    public String getSalt(){
        return getValue(SALT_PROPERTY, SALT_PATH);
    }

    private String getValue(String param, String filePath){

        String result = null;

        try (InputStream inputStream = inputStreamWrapper
                .getFileInputStream(filePath)) {

            properties.load(inputStream);

            result = properties.getProperty(param);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }



}
