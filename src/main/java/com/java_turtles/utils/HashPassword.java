package com.java_turtles.utils;

import com.java_turtles.exceptions.HashPasswordNullException;
import com.java_turtles.wrappers.MessageDigestWrapper;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashPassword {

    private PropertiesUtil propertiesUtil;
    private MessageDigestWrapper messageDigestWrapper;

    public HashPassword(PropertiesUtil propertiesUtil, MessageDigestWrapper messageDigestWrapper) {
        this.propertiesUtil = propertiesUtil;
        this.messageDigestWrapper = messageDigestWrapper;
    }

    public String hashPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, HashPasswordNullException {

        if (password == null||password.isEmpty()){
            throw new HashPasswordNullException();
        }

        String salt = propertiesUtil.getSalt();

        byte[] bytes = hash(password, salt);

        StringBuilder sp = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            sp.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sp.toString().toUpperCase();
    }

    private byte[] hash(String passwordToHash, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = messageDigestWrapper.getWithInstance();
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        md.update(passwordToHash.getBytes(StandardCharsets.UTF_8));
        return md.digest();

    }
}
