package com.java_turtles.utils.db_utils;

import com.java_turtles.models.User;

public interface IUsersDbUtility {

    boolean addUser(User user);

    boolean isUniqueLogin(String login); // наличие уникального логина - если логин есть, то ошибка

    boolean isUniqueEmail(String email);

    boolean confirmUserByLogin(String login, String password);

    boolean confirmUserByEmail(String email, String password);

}
