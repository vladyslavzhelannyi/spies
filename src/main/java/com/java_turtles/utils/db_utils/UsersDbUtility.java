package com.java_turtles.utils.db_utils;

import com.java_turtles.models.User;

public class UsersDbUtility implements IUsersDbUtility {

    @Override
    public boolean addUser(User user) {
        return true;
    }

    @Override
    public boolean isUniqueLogin(String login) {
        return true;
    }

    @Override
    public boolean isUniqueEmail(String email) {
        return true;
    }

    @Override
    public boolean confirmUserByLogin(String login, String password) {
        return false;
    }

    @Override
    public boolean confirmUserByEmail(String email, String password) {
        return false;
    }
}
