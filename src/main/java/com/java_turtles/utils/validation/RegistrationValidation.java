package com.java_turtles.utils.validation;

import com.java_turtles.models.entities.RegistrationDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.java_turtles.constants.ValidationConstants.*;

public class RegistrationValidation {

    private IUsersDbUtility dbUtility;

    private String login;
    private String password;
    private String confirmPassword;
    private String email;
    private String phoneNumber;
    private String companyName;

    public RegistrationValidation(IUsersDbUtility dbUtility) {
        this.dbUtility = dbUtility;
    }

    public String getValidateResponse(RegistrationDTO regDTO){

        if(regDTO == null){
            return MSG_INVALID_NULL;
        }

        login = regDTO.getLogin();
        password = regDTO.getPassword();
        confirmPassword = regDTO.getConfirmPassword();
        email = regDTO.getEmail();
        phoneNumber = regDTO.getPhoneNumber();
        companyName = regDTO.getCompany();

        return getMessage();

    }

    private String getMessage(){

        if(hasNullField()){
            return MSG_INVALID_NULL;
        }

        if(hasEmptyField()){
            return MSG_INVALID_EMPTY_FIELD;
        }

        if (!isCorrectLogin(login)) {
            return MSG_INVALID_INCORRECT_LOGIN;
        }

        if (!isCorrectPassword(password)) {
            return MSG_INVALID_INCORRECT_PASSWORD;
        }

        if (!isPasswordConfirm(password, confirmPassword)){
            return MSG_INVALID_CONFIRM_PASSWORD_MISMATCH;
        }

        if (!isCorrectEmail(email)) {
            return MSG_INVALID_INCORRECT_EMAIL;
        }

        if (!isCorrectPhoneNumber(phoneNumber)) {
            return MSG_INVALID_INCORRECT_NUMBER;
        }

        if (!isCorrectCompanyName(companyName)){
            return MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS;
        }

        return MSG_SUCCESS;
    }

    private boolean hasNullField(){
        boolean anyFieldNull = login == null||password == null||confirmPassword == null||email == null||phoneNumber == null;
        return anyFieldNull;
    }

    private boolean hasEmptyField(){
        boolean anyFieldEmpty = login.isBlank()||password.isBlank()||confirmPassword.isBlank()||email.isBlank()||phoneNumber.isBlank();
        return anyFieldEmpty;
    }

    private boolean isCorrect(String checkField, String regex){

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(checkField);

        return matcher.matches();
    }

    private boolean isCorrectLogin(String login){
        if (isCorrect(login, REGEX_CORRECT_LOGIN)){
            return  dbUtility.isUniqueLogin(login);
        }
        return false;
    }

    private boolean isCorrectPassword(String password){
        if (isCorrect(password, REGEX_CORRECT_PASSWORD)){
            return isCorrect(password, REGEX_CORRECT_PASSWORD_2);
        }
        return false;
    }

    private boolean isPasswordConfirm(String password, String confirmPassword){
        return password.equals(confirmPassword);
    }

    private boolean isCorrectEmail(String email){

        if (isCorrect(email, REGEX_CORRECT_EMAIL)){
            return dbUtility.isUniqueEmail(email);
        }

        return false;
    }

    private boolean isCorrectPhoneNumber(String phoneNumber){
        if (isCorrect(phoneNumber, REGEX_CORRECT_PHONE_NUMBER)){
            return true; //проверка в базе
        }
        return false;
    }

    private boolean isCorrectCompanyName(String companyName){
        return companyName.length() <= COMPANY_NAME_MAX_SYMBOLS;
    }

}
