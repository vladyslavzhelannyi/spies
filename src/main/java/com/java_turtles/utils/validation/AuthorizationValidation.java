package com.java_turtles.utils.validation;

import com.java_turtles.models.entities.AuthDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.java_turtles.constants.ValidationConstants.*;

public class AuthorizationValidation {
    private IUsersDbUtility usersDbUtility;
    private String login;
    private String password;
    boolean isEmail;

    public AuthorizationValidation(IUsersDbUtility usersDbUtility) {
        this.usersDbUtility = usersDbUtility;
    }

    public String getValidateResponse(AuthDTO authDTO) {
        if (authDTO == null) {
            return MSG_INVALID_NULL;
        }

        login = authDTO.getLogin();
        password = authDTO.getPassword();

        return getMessage();
    }

    private String getMessage(){

        if(hasNullField()){
            return MSG_INVALID_NULL;
        }

        if(hasEmptyField()){
            return MSG_INVALID_EMPTY_FIELD;
        }

        boolean bool = !isCorrectLogin(login);
        if (bool) {
            return MSG_INVALID_INCORRECT_LOGIN;
        }

        if (!isCorrectPassword(password)) {
            return MSG_INVALID_INCORRECT_PASSWORD;
        }

        return MSG_SUCCESS;
    }

    boolean hasNullField(){
        if (login == null || password == null) {
            return true;
        }
        return false;
    }

    boolean hasEmptyField(){
        if (login.isBlank() || password.isBlank()) {
            return true;
        }
        return false;
    }

    private boolean isCorrect(String checkField, String regex){

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(checkField);

        return matcher.matches();
    }

    private boolean isCorrectLogin(String login){
        if (isCorrect(login, REGEX_CORRECT_LOGIN)){
            isEmail = false;
        }
        else if (isCorrect(login, REGEX_CORRECT_EMAIL)) {
            isEmail = true;
        }
        else {
            return false;
        }

        if (isEmail) {
            return usersDbUtility.isUniqueEmail(login);
        }
        else {
            return usersDbUtility.isUniqueLogin(login);
        }
    }

    private boolean isCorrectPassword(String password){
        if (!isCorrect(password, REGEX_CORRECT_PASSWORD)){
            return false;
        }

        if (!isCorrect(password, REGEX_CORRECT_PASSWORD_2)) {
            return false;
        }

        if (isEmail) {
            return usersDbUtility.confirmUserByEmail(login, password);
        }
        else {
            return usersDbUtility.confirmUserByLogin(login, password);
        }
    }

}
