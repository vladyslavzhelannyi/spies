package com.java_turtles.models.entities;

public class RegistrationDTO {

    private String login;
    private String password;
    private String confirmPassword;
    private String email;
    private String phoneNumber;
    private String company;

    public RegistrationDTO() {
    }

    public RegistrationDTO(String login, String password, String confirmPassword, String email, String phoneNumber, String company) {
        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.company = company;
    }


    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCompany() {
        return company;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
