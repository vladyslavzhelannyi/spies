package com.java_turtles.models.entities;

public class RecoverDTO {

    private String email;

    public RecoverDTO(String email) {
        this.email = email;
    }

    public RecoverDTO() {
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "RecoverDto{" +
                "email='" + email + '\'' +
                '}';
    }

}
