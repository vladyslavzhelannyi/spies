package com.java_turtles.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.controllers.RegController;
import com.java_turtles.models.User;
import com.java_turtles.models.entities.RegistrationDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;
import com.java_turtles.utils.db_utils.UsersDbUtility;
import com.java_turtles.utils.validation.RegistrationValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static testdata.ControllerTestData.RegControllerTestData.*;

public class RegControllerTest {


    private static ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
    private static RegistrationValidation registrationValidation = Mockito.mock(RegistrationValidation.class);
    private static BufferedReader reader = Mockito.mock(BufferedReader.class);
    private static Stream<String> stream = Mockito.mock(Stream.class);
    private static PrintWriter writer;
    private static Collector collector = Mockito.mock(Collector.class);
    private static IUsersDbUtility usersDbUtility = Mockito.mock(UsersDbUtility.class);

    private static HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    private static HttpServletResponse response;


    private static RegController cut = new RegController(objectMapper, registrationValidation, usersDbUtility);

    @BeforeEach
    void setUp(){
        writer = Mockito.mock(PrintWriter.class);
        response = Mockito.mock(HttpServletResponse.class);
    }


    static Arguments[] doPostTestArgs(){
        return new Arguments[]{

                Arguments.arguments(CORRECT_BODY_1, CORRECT_REG_DTO_1, MSG_SUCCESS, USER_1, true, 1, 1, 201),
                Arguments.arguments(CORRECT_BODY_2, CORRECT_REG_DTO_2, MSG_SUCCESS, USER_2, true, 1, 1, 201),
                Arguments.arguments(CORRECT_BODY_2, CORRECT_REG_DTO_2, MSG_SUCCESS, USER_2, true, 1, 1, 201),
                Arguments.arguments(BODY_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, null, false, 1, 1, 400),
                Arguments.arguments(BODY_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_1, MSG_INVALID_INCORRECT_PASSWORD, null, false, 1, 1, 400),
                Arguments.arguments(BODY_INCORRECT_CONFIRM, INCORRECT_CONFIRM_REG_DTO_1, MSG_INVALID_CONFIRM_PASSWORD_MISMATCH, null, false, 1, 1, 400),
                Arguments.arguments(BODY_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_1, MSG_INVALID_INCORRECT_EMAIL, null, false, 1, 1, 400),
                Arguments.arguments(BODY_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_1, MSG_INVALID_INCORRECT_NUMBER, null, false, 1, 1, 400),
                Arguments.arguments(BODY_INCORRECT_COMPANY, INCORRECT_COMPANY_NAME_REG_DTO_1, MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS, null, false, 1, 1, 400),

        };
    }

    @MethodSource("doPostTestArgs")
    @ParameterizedTest
    void doPostTest(String body, RegistrationDTO regDTO,
                    String validateResponse, User user, boolean add, int times1, int times2, int status) throws IOException, ServletException {

        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {

            Mockito.when(request.getReader()).thenReturn(reader);
            Mockito.when(reader.lines()).thenReturn(stream);
            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);
            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);
            Mockito.when(objectMapper.readValue(body, RegistrationDTO.class)).thenReturn(regDTO);
            Mockito.when(registrationValidation.getValidateResponse(regDTO)).thenReturn(validateResponse);

            Mockito.when(response.getWriter()).thenReturn(writer);
            Mockito.when(usersDbUtility.addUser(user)).thenReturn(add);

            cut.doPost(request, response);

            Mockito.verify(writer, Mockito.times(times1)).write(validateResponse);
            Mockito.verify(response, Mockito.times(times2)).setStatus(status);
        }

    }

    static Arguments[] doPostExceptionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(BODY_EXCEPTION,
                        RESPONSE_INCORRECT_DATA, 1, HttpServletResponse.SC_BAD_REQUEST)
        };
    }

    @MethodSource("doPostExceptionTestArgs")
    @ParameterizedTest
    void doPostExceptionTest(String body, String responseIncorrectData, int times, int status) throws IOException, ServletException {

        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {
            Mockito.when(request.getReader()).thenReturn(reader);

            Mockito.when(reader.lines()).thenReturn(stream);

            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);

            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);

            Mockito.when(request.getReader().lines().collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);

            Mockito.when(objectMapper.readValue(body, RegistrationDTO.class)).thenThrow(JsonProcessingException.class);

            Mockito.when(response.getWriter()).thenReturn(writer);

            cut.doPost(request, response);

            Mockito.verify(writer, Mockito.times(times)).write(responseIncorrectData);
            Mockito.verify(response, Mockito.times(times)).setStatus(status);
            // Mockito.when(registrationValidation.getValidateResponse(regDTO)).thenReturn(validateResponse);
        }
    }

}
