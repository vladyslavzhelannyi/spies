package com.java_turtles.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_turtles.controllers.AuthorizationController;
import com.java_turtles.models.entities.AuthDTO;
import com.java_turtles.utils.validation.AuthorizationValidation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.java_turtles.constants.ValidationConstants.MSG_INCORRECT_DATA;
import static com.java_turtles.constants.ValidationConstants.MSG_SUCCESS;
import static testdata.ControllerTestData.AuthorizationControllerTestData.*;

public class AuthorizationControllerTest {
    ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
    AuthorizationValidation authorizationValidation = Mockito.mock(AuthorizationValidation.class);
    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);
    BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
    Stream<String> stream = Mockito.mock(Stream.class);
    Collector collector = Mockito.mock(Collector.class);
    PrintWriter printWriter = Mockito.mock(PrintWriter.class);

    AuthorizationController cut = new AuthorizationController(objectMapper, authorizationValidation);

    static Arguments[] doPostTestArgs() {
        return new Arguments[] {
                Arguments.arguments(BODY_1, AUTH_DTO_1, VALIDATE_RESPONSE_1, RESPONSE_OK_TIMES_1, INCORRECT_DATA_TIMES_1,
                        VALIDATE_RESPONSE_TIMES_1, BAD_REQUEST_TIMES_1),
                Arguments.arguments(BODY_2, AUTH_DTO_2, VALIDATE_RESPONSE_2, RESPONSE_OK_TIMES_2, INCORRECT_DATA_TIMES_2,
                        VALIDATE_RESPONSE_TIMES_2, BAD_REQUEST_TIMES_2),
        };
    }

    @ParameterizedTest
    @MethodSource("doPostTestArgs")
    void doPostTest(String body, AuthDTO authDTO, String validateResponse, int responseOkTimes, int incorrectDataTimes,
                    int validateResponseTimes, int badRequestTimes) throws ServletException, IOException {
        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {
            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);
            Mockito.when(req.getReader()).thenReturn(bufferedReader);
            Mockito.when(bufferedReader.lines()).thenReturn(stream);
            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);
            Mockito.when(objectMapper.readValue(body, AuthDTO.class)).thenReturn(authDTO);
            Mockito.when(authorizationValidation.getValidateResponse(authDTO)).thenReturn(validateResponse);
            Mockito.when(resp.getWriter()).thenReturn(printWriter);

            cut.doPost(req, resp);

            Mockito.verify(printWriter, Mockito.times(incorrectDataTimes)).write(MSG_INCORRECT_DATA);
            Mockito.verify(printWriter, Mockito.times(validateResponseTimes)).write(validateResponse);
            Mockito.verify(resp, Mockito.times(badRequestTimes)).setStatus(HttpServletResponse.SC_BAD_REQUEST);
            Mockito.verify(resp, Mockito.times(responseOkTimes)).setStatus(HttpServletResponse.SC_OK);
        }
    }

    @Test
    void doPostExceptionTest() throws IOException, ServletException {
        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {
            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);
            Mockito.when(req.getReader()).thenReturn(bufferedReader);
            Mockito.when(bufferedReader.lines()).thenReturn(stream);
            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(BODY_3);
            Mockito.when(objectMapper.readValue(BODY_3, AuthDTO.class)).thenThrow(new RuntimeException());
            Mockito.when(resp.getWriter()).thenReturn(printWriter);

            cut.doPost(req, resp);

            Mockito.verify(printWriter, Mockito.times(1)).write(MSG_INCORRECT_DATA);
            Mockito.verify(resp, Mockito.times(1)).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
