package com.java_turtles.utils;

import com.java_turtles.exceptions.HashPasswordNullException;
import com.java_turtles.wrappers.MessageDigestWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static testdata.HashPasswordTestData.*;

public class HashPasswordTest {

     MessageDigestWrapper messageDigestWrapper = Mockito.mock(MessageDigestWrapper.class);
     MessageDigest md = Mockito.mock(MessageDigest.class);
     PropertiesUtil propertiesUtil = Mockito.mock(PropertiesUtil.class);

     HashPassword cut = new HashPassword(propertiesUtil, messageDigestWrapper);

    private static final String SALT = "FjSO&@MPH3D21GUdq99wHv24po_OLpt88nzitWLOUGY@PYPH";

    static Arguments[] hashPasswordTestArgs(){
        return new Arguments[]{
                Arguments.arguments(PASSWORD_1, DIGEST_1, HASHED_PASSWORD_1),
                Arguments.arguments(PASSWORD_2, DIGEST_2, HASHED_PASSWORD_2),
//                Arguments.arguments(PASSWORD_EMPTY, null, null),
//                Arguments.arguments(PASSWORD_1, DIGEST_1, HASHED_PASSWORD_1),
        };
    }

    @MethodSource("hashPasswordTestArgs")
    @ParameterizedTest
    void hashPasswordTest(String password, byte[] digest, String expected) throws NoSuchAlgorithmException, UnsupportedEncodingException, HashPasswordNullException {
        Mockito.when(propertiesUtil.getSalt()).thenReturn(SALT);
        Mockito.when(messageDigestWrapper.getWithInstance()).thenReturn(md);
        Mockito.when(md.digest()).thenReturn(digest);

        String actual = cut.hashPassword(password);

        Mockito.verify(md, Mockito.times(1)).update(SALT.getBytes(StandardCharsets.UTF_8));
        Mockito.verify(md, Mockito.times(1)).update(password.getBytes(StandardCharsets.UTF_8));

        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] hashPasswordExceptionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(PASSWORD_EMPTY),
                Arguments.arguments(PASSWORD_NULL)
        };
    }

    @MethodSource("hashPasswordExceptionTestArgs")
    @ParameterizedTest
    void hashPasswordExceptionTest(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, HashPasswordNullException {

        Throwable thrown = Assertions.assertThrows(HashPasswordNullException.class, () -> {
            cut.hashPassword(password);
        });
        Assertions.assertNotNull(thrown.getMessage());

    }



}
