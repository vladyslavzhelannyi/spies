package com.java_turtles.utils;

import com.java_turtles.wrappers.InputStreamWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.Mockito;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static com.java_turtles.constants.PropertiesConstants.*;

public class PropertiesUtilTest {
    private static final InputStreamWrapper INPUT_STREAM_WRAPPER = Mockito.mock(InputStreamWrapper.class);
    private static final Properties PROPERTIES = Mockito.mock(Properties.class);
    private static final FileInputStream INPUT_STREAM = Mockito.mock(FileInputStream.class);
    private static final int LOAD_TIMES = 1;
    private static final String PROPERTY_KEY = "propertyKey";

    private static final PropertiesUtil cut = new PropertiesUtil(INPUT_STREAM_WRAPPER, PROPERTIES);

    @Test
    void getJwtSecretKeyTest() throws IOException {
        Mockito.when(INPUT_STREAM_WRAPPER.getFileInputStream(PROPERTIES_PATH))
                .thenReturn(INPUT_STREAM);
        Mockito.when(PROPERTIES.getProperty(JWT_PROPERTY)).thenReturn(PROPERTY_KEY);

        String actual = cut.getJwtSecretKey();

        Mockito.verify(PROPERTIES, Mockito.times(LOAD_TIMES)).load(INPUT_STREAM);
        Assertions.assertEquals(PROPERTY_KEY, actual);
    }



    @Test
    void getJwtSecretKeyExceptionTest() throws FileNotFoundException {
        Mockito.when(INPUT_STREAM_WRAPPER.getFileInputStream("src/main/resources/config.properties"))
                .thenAnswer(invocationOnMock -> {throw new IOException();});
        String actual = cut.getJwtSecretKey();
        Assertions.assertNull(actual);
    }

    @Test
    void getSaltTest() throws IOException {
        Mockito.when(INPUT_STREAM_WRAPPER.getFileInputStream(SALT_PATH))
                .thenReturn(INPUT_STREAM);
        Mockito.when(PROPERTIES.getProperty(SALT_PROPERTY)).thenReturn(PROPERTY_KEY);

        String actual = cut.getSalt();

        Mockito.verify(PROPERTIES, Mockito.times(LOAD_TIMES)).load(INPUT_STREAM);
        Assertions.assertEquals(PROPERTY_KEY, actual);
    }



    @Test
    void getSaltExceptionTest() throws FileNotFoundException {
        Mockito.when(INPUT_STREAM_WRAPPER.getFileInputStream(SALT_PATH))
                .thenAnswer(invocationOnMock -> {throw new IOException();});
        String actual = cut.getSalt();
        Assertions.assertNull(actual);
    }

}
