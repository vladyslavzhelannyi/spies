package com.java_turtles.utils.validation;


import com.java_turtles.models.entities.AuthDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import static com.java_turtles.constants.ValidationConstants.*;
import static testdata.ValidationTestData.AuthorizationValidationTestData.*;

public class AuthorizationValidationTest {
    IUsersDbUtility usersDbUtility = Mockito.mock(IUsersDbUtility.class);
    AuthorizationValidation cut = new AuthorizationValidation(usersDbUtility);

    static Arguments[] getValidateResponseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(CORRECT_DTO_1, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_DTO_1, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(CORRECT_DTO_1, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(CORRECT_DTO_2, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_DTO_3, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_DTO_4, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_TRUE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_DTO_4, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(CORRECT_DTO_4, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_TRUE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_1, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_DTO_2, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_DTO_3, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_DTO_4, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_DTO_5, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_6, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_7, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_8, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_9, CHECKED_LOGIN_TRUE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_DTO_10, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_EMPTY_FIELD),
                Arguments.arguments(INCORRECT_DTO_11, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_EMPTY_FIELD),
                Arguments.arguments(INCORRECT_DTO_12, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_NULL),
                Arguments.arguments(INCORRECT_DTO_13, CHECKED_LOGIN_FALSE, CHECKED_EMAIL_FALSE,
                        CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_NULL),

        };
    }

    @MethodSource("getValidateResponseTestArgs")
    @ParameterizedTest
    void getValidateResponseTest(AuthDTO authDTO, boolean checkedLogin, boolean checkedEmail, boolean confirmedByLogin,
                                 boolean confirmedByEmail, String expected){
        Mockito.when(usersDbUtility.isUniqueLogin(authDTO.getLogin())).thenReturn(checkedLogin);
        Mockito.when(usersDbUtility.isUniqueEmail(authDTO.getLogin())).thenReturn(checkedEmail);
        Mockito.when(usersDbUtility.confirmUserByLogin(authDTO.getLogin(), authDTO.getPassword()))
                .thenReturn(confirmedByLogin);
        Mockito.when(usersDbUtility.confirmUserByEmail(authDTO.getLogin(), authDTO.getPassword()))
                .thenReturn(confirmedByEmail);

        String actual = cut.getValidateResponse(authDTO);

        Assertions.assertEquals(expected, actual);
    }
}
