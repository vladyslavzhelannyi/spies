//package com.java_turtles.utils.validation;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//
//import static com.java_turtles.constants.RegistrationConstants.*;
//
//public class RegistrationValidationTest {
//    RegistrationValidation cut = new RegistrationValidation();
//
//    static Arguments[] validateLoginTestArgs() {
//        return new Arguments[] {
//            Arguments.arguments("qwert45H", VALIDATED),
//            Arguments.arguments("", EMPTY_INPUT),
//            Arguments.arguments("qwert45Hit659jtd", VALIDATED),
//            Arguments.arguments("qwer", INCORRECT_LOGIN_LENGTH),
//            Arguments.arguments("qwer2342fefef23423f23", INCORRECT_LOGIN_LENGTH),
//            Arguments.arguments("qwedwedwf$@", INCORRECT_CHARACTERS),
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validateLoginTestArgs")
//    void validateLoginTest(String login, String expected) {
//        String actual = cut.validateLogin(login);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    static Arguments[] validatePasswordTestArgs() {
//        return new Arguments[] {
//                Arguments.arguments("qwerty12", VALIDATED),
//                Arguments.arguments("QwertyQwerty", AT_LEAST_1NUMBER),
//                Arguments.arguments("1234512345", AT_LEAST_1LETTER),
//                Arguments.arguments("qweqwe1", INCORRECT_PASS_LENGTH),
//                Arguments.arguments("123qwerty123qwerty", INCORRECT_PASS_LENGTH),
//                Arguments.arguments("qwedwqr#!@$", INCORRECT_CHARACTERS),
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validatePasswordTestArgs")
//    void validatePasswordTest(String password, String expected) {
//        String actual = cut.validatePassword(password);
//        Assertions.assertEquals(expected, actual);
//    }
//}
