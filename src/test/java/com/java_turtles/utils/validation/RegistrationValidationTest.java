package com.java_turtles.utils.validation;

import com.java_turtles.models.entities.RegistrationDTO;
import com.java_turtles.utils.db_utils.IUsersDbUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import static com.java_turtles.constants.ValidationConstants.*;
import static testdata.ValidationTestData.RegistrationValidationTestData.*;


public class RegistrationValidationTest {

    private static IUsersDbUtility iUsersDbUtility = Mockito.mock(IUsersDbUtility.class);

    RegistrationValidation cut = new RegistrationValidation(iUsersDbUtility);

    static Arguments[] getValidateResponseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(CORRECT_REG_DTO_1, MSG_SUCCESS, CORRECT_REG_DTO_1.getLogin(), CORRECT_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(CORRECT_REG_DTO_2, MSG_SUCCESS, CORRECT_REG_DTO_2.getLogin(), CORRECT_REG_DTO_2.getEmail(), true, true),
                Arguments.arguments(CORRECT_REG_DTO_3, MSG_SUCCESS, CORRECT_REG_DTO_3.getLogin(), CORRECT_REG_DTO_3.getEmail(), true, true),

                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_1.getLogin(),INCORRECT_LOGIN_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_2, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_2.getLogin(),INCORRECT_LOGIN_REG_DTO_2.getEmail(), true, true),
                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_3, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_3.getLogin(),INCORRECT_LOGIN_REG_DTO_3.getEmail(), true, true),

                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_1, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_1.getLogin(), INCORRECT_PASSWORD_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_2, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_2.getLogin(), INCORRECT_PASSWORD_REG_DTO_2.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_3, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_3.getLogin(), INCORRECT_PASSWORD_REG_DTO_3.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_4, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_4.getLogin(), INCORRECT_PASSWORD_REG_DTO_4.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_5, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_5.getLogin(), INCORRECT_PASSWORD_REG_DTO_5.getEmail(), true, true),

                Arguments.arguments(INCORRECT_CONFIRM_REG_DTO_1, MSG_INVALID_CONFIRM_PASSWORD_MISMATCH, INCORRECT_CONFIRM_REG_DTO_1.getLogin(), INCORRECT_CONFIRM_REG_DTO_1.getEmail(), true, true),

                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_1, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_1.getLogin(), INCORRECT_EMAIL_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_2, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_2.getLogin(), INCORRECT_EMAIL_REG_DTO_2.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_3, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_3.getLogin(), INCORRECT_EMAIL_REG_DTO_3.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_4, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_4.getLogin(), INCORRECT_EMAIL_REG_DTO_4.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_5, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_5.getLogin(), INCORRECT_EMAIL_REG_DTO_5.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_6, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_6.getLogin(), INCORRECT_EMAIL_REG_DTO_6.getEmail(), true, true),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_7, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_7.getLogin(), INCORRECT_EMAIL_REG_DTO_7.getEmail(), true, true),

                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_1, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_1.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_2, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_2.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_2.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_3, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_3.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_3.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_4, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_4.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_4.getEmail(), true, true),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_5, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_5.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_5.getEmail(), true, true),

                Arguments.arguments(INCORRECT_COMPANY_NAME_REG_DTO_1, MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS, INCORRECT_COMPANY_NAME_REG_DTO_1.getLogin(), INCORRECT_COMPANY_NAME_REG_DTO_1.getEmail(), true, true),

                Arguments.arguments(INCORRECT_NULL_REG_DTO_1, MSG_INVALID_NULL, INCORRECT_NULL_REG_DTO_1.getLogin(), INCORRECT_NULL_REG_DTO_1.getEmail(), true, true),
                Arguments.arguments(null, MSG_INVALID_NULL, "", "", true, true),

                Arguments.arguments(INCORRECT_EMPTY_REG_DTO_1, MSG_INVALID_EMPTY_FIELD, "", "", true, true),

                Arguments.arguments(CORRECT_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, CORRECT_REG_DTO_1.getLogin(), CORRECT_REG_DTO_1.getEmail(), false, true),
                Arguments.arguments(CORRECT_REG_DTO_2, MSG_INVALID_INCORRECT_EMAIL, CORRECT_REG_DTO_2.getLogin(), CORRECT_REG_DTO_2.getEmail(), true, false)

        };
    }

    @MethodSource("getValidateResponseTestArgs")
    @ParameterizedTest
    void getValidateResponseTest(RegistrationDTO regDTO, String expected, String login, String email, boolean uniqueLog, boolean uniqueEmail){

        Mockito.when(iUsersDbUtility.isUniqueLogin(login)).thenReturn(uniqueLog);
        Mockito.when(iUsersDbUtility.isUniqueEmail(email)).thenReturn(uniqueEmail);

        String actual = cut.getValidateResponse(regDTO);

        Assertions.assertEquals(expected, actual);

    }

}
