package testdata;

public class HashPasswordTestData {

    public static final String PASSWORD_1 = "Password2022";
    public static final byte[] DIGEST_1 = {103, 106, -95, -109, 67, 106, 51, 26, 72, 125, -119, -46, 52, -84, 16, -80, 5, 109, -59, 83, 68, -115, -75, -67, 26, -119, 89, 69, 94, -67, -122, 57, 41, 54, 75, -28, -9, 8, -62, -121, 4, -112, -85, 101, 8, 21, -52, -82, 85, -125, -117, -84, -10, -68, -31, -5, 51, -59, -18, -110, -49, 106, -46, -122};
    public static final String HASHED_PASSWORD_1 = "676AA193436A331A487D89D234AC10B0056DC553448DB5BD1A8959455EBD863929364BE4F708C2870490AB650815CCAE55838BACF6BCE1FB33C5EE92CF6AD286";

    public static final String PASSWORD_2 = "30SecondToMars";
    public static final byte[] DIGEST_2 = {-49, 71, -61, -14, 111, -120, 30, 6, 23, 78, -38, -73, 56, 18, 26, 19, 107, -112, 126, -29, -34, 55, -65, 61, -122, -73, 41, -73, 103, -91, -122, 66, -24, 114, 23, -69, 43, 57, 50, 58, 82, -2, 69, 120, -9, -29, 78, 87, -3, -38, 112, -37, -63, 102, -69, 108, -24, 89, -12, -48, 66, 36, 95, -103};
    public static final String HASHED_PASSWORD_2 = "CF47C3F26F881E06174EDAB738121A136B907EE3DE37BF3D86B729B767A58642E87217BB2B39323A52FE4578F7E34E57FDDA70DBC166BB6CE859F4D042245F99";

    public static final String PASSWORD_EMPTY = "";
    public static final String PASSWORD_NULL = null;
}
