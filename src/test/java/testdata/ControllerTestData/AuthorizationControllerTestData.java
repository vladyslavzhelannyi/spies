package testdata.ControllerTestData;

import com.java_turtles.models.entities.AuthDTO;

import static com.java_turtles.constants.ValidationConstants.*;

public class AuthorizationControllerTestData {
    public static final String BODY_1 = "{\"login\":\"log\",\"password\":\"pass\"}";
    public static final String BODY_2 = "{\"login\":\"bad log\",\"password\":\"bad pass\"}";
    public static final String BODY_3 = "not serializable string";
    public static final AuthDTO AUTH_DTO_1 = new AuthDTO("log", "pass");
    public static final AuthDTO AUTH_DTO_2 = new AuthDTO("bad log", "bad pass");
    public static final String VALIDATE_RESPONSE_1 = MSG_SUCCESS;
    public static final String VALIDATE_RESPONSE_2 = MSG_INVALID_INCORRECT_LOGIN;
    public static final int RESPONSE_OK_TIMES_1 = 1;
    public static final int RESPONSE_OK_TIMES_2 = 0;
    public static final int INCORRECT_DATA_TIMES_1 = 0;
    public static final int INCORRECT_DATA_TIMES_2 = 0;
    public static final int VALIDATE_RESPONSE_TIMES_1 = 0;
    public static final int VALIDATE_RESPONSE_TIMES_2 = 1;
    public static final int BAD_REQUEST_TIMES_1 = 0;
    public static final int BAD_REQUEST_TIMES_2 = 1;
}
