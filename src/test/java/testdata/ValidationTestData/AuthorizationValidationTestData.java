package testdata.ValidationTestData;

import com.java_turtles.models.entities.AuthDTO;

public class AuthorizationValidationTestData {
    private static final String CORRECT_LOGIN_1 = "loginUser";
    private static final String CORRECT_LOGIN_2 = "login21User";
    private static final String CORRECT_LOGIN_3 = "987654321";
    private static final String CORRECT_LOGIN_4 = "useremail@gmail.com";
    private static final String INCORRECT_LOGIN_1 = "$user#login";
    private static final String INCORRECT_LOGIN_2 = "user";
    private static final String INCORRECT_LOGIN_3 = "TooLongUserLoginForAuth";
    private static final String INCORRECT_LOGIN_4 = "useremail@gmail";
    private static final String INCORRECT_LOGIN_5 = "";
    private static final String INCORRECT_LOGIN_6 = null;

    private static final String CORRECT_PASSWORD = "Password007";
    private static final String INCORRECT_PASSWORD_1 = "UserPassword";
    private static final String INCORRECT_PASSWORD_2 = "password007";
    private static final String INCORRECT_PASSWORD_3 = "PAS12";
    private static final String INCORRECT_PASSWORD_4 = "Password123456789TooLong";
    private static final String INCORRECT_PASSWORD_5 = "PASSWORD1";
    private static final String INCORRECT_PASSWORD_6 = "";
    private static final String INCORRECT_PASSWORD_7 = null;

    public static final AuthDTO CORRECT_DTO_1 = new AuthDTO(CORRECT_LOGIN_1, CORRECT_PASSWORD);
    public static final AuthDTO CORRECT_DTO_2 = new AuthDTO(CORRECT_LOGIN_2, CORRECT_PASSWORD);
    public static final AuthDTO CORRECT_DTO_3 = new AuthDTO(CORRECT_LOGIN_3, CORRECT_PASSWORD);
    public static final AuthDTO CORRECT_DTO_4 = new AuthDTO(CORRECT_LOGIN_4, CORRECT_PASSWORD);

    public static final AuthDTO INCORRECT_DTO_1 = new AuthDTO(INCORRECT_LOGIN_1, CORRECT_PASSWORD);
    public static final AuthDTO INCORRECT_DTO_2 = new AuthDTO(INCORRECT_LOGIN_2, CORRECT_PASSWORD);
    public static final AuthDTO INCORRECT_DTO_3 = new AuthDTO(INCORRECT_LOGIN_3, CORRECT_PASSWORD);
    public static final AuthDTO INCORRECT_DTO_4 = new AuthDTO(INCORRECT_LOGIN_4, CORRECT_PASSWORD);

    public static final AuthDTO INCORRECT_DTO_5 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_1);
    public static final AuthDTO INCORRECT_DTO_6 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_2);
    public static final AuthDTO INCORRECT_DTO_7 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_3);
    public static final AuthDTO INCORRECT_DTO_8 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_4);
    public static final AuthDTO INCORRECT_DTO_9 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_5);

    public static final AuthDTO INCORRECT_DTO_10 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_6);
    public static final AuthDTO INCORRECT_DTO_11 = new AuthDTO(INCORRECT_LOGIN_5, CORRECT_PASSWORD);
    public static final AuthDTO INCORRECT_DTO_12 = new AuthDTO(INCORRECT_LOGIN_6, CORRECT_PASSWORD);
    public static final AuthDTO INCORRECT_DTO_13 = new AuthDTO(CORRECT_LOGIN_1, INCORRECT_PASSWORD_7);

    public static final boolean CHECKED_EMAIL_TRUE = true;

    public static final boolean CHECKED_EMAIL_FALSE = false;

    public static final boolean CHECKED_LOGIN_TRUE = true;

    public static final boolean CHECKED_LOGIN_FALSE = false;

    public static final boolean CONFIRMED_USER_BY_EMAIL_TRUE = true;

    public static final boolean CONFIRMED_USER_BY_EMAIL_FALSE = false;

    public static final boolean CONFIRMED_USER_BY_LOGIN_TRUE = true;

    public static final boolean CONFIRMED_USER_BY_LOGIN_FALSE = false;
}
